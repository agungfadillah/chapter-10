import React from "react";
import { connect } from "react-redux";
import { updateScore, setUserChoice } from "./gameActions";
import { Container, Row, Col, Nav, NavItem } from "react-bootstrap";
import { useEffect, useState } from "react";
import back from "../../public/back.png";
import logo from "../../public/logo1.png";
import refresh from "../../public/refresh.png";
import Image from "next/image";
import Link from "next/link";


export default function GamePage() {
const [pick, setPick] = useState("");
const [computerPicks, setCom] = useState("");
const [result, setResult] = useState("");


const Game = ({ score, updateScore, setUserChoice }) => {
  const handlePicks = (pick) => {
      const picks = ["batu", "gunting", "kertas"];
      const computerPicks = picks[Math.floor(Math.random() * picks.length)];

      setPick(pick);
      setCom(computerPicks);
      setUserChoice(pick);
      connectUser()
      
      if (computerPicks === "batu") {
      document.getElementById("batu2").style.background=
      "#C4C4C4";
      document.getElementById("gunting2").style.background=
      "#9C835F";
      document.getElementById("kertas2").style.background=
      "#9C835F";
      }
      else if (computerPicks === "gunting") {
      document.getElementById("gunting2").style.background=
      "#C4C4C4";
      document.getElementById("kertas2").style.background=
      "#9C835F";
      document.getElementById("batu2").style.background=
      "#9C835F";
      } 
      else {
      document.getElementById("kertas2").style.background=
      "#C4C4C4";
      document.getElementById("gunting2").style.background=
      "#9C835F";
      document.getElementById("batu2").style.background=
      "#9C835F";
      }

      console.log(`player: ${pick}`);
      console.log(`com: ${computerPicks}`);
      console.log(`hasil: ${result}`);
    };
    


useEffect(() => {
    if (pick && computerPicks) {
      if (pick === computerPicks) {
        setResult("draw");
      } else if 
        ((pick === "batu" && computerPicks === "gunting") ||
         (pick === "kertas" && computerPicks === "batu") || 
         (pick === "gunting" && computerPicks === "kertas")) {
        setResult("player win");
        updateScore(1);
      } else {
        setResult("com win");
        updateScore(1);
      }
    }
  }, [pick, computerPicks]);


  useEffect(() => {
    const replace = document.querySelector('.replace');
      if (result === "draw") {
          replace.innerHTML = 'DRAW';
          replace.classList.add('result');
          replace.style.background = '#035B0C';
          if (replace.classList.contains('versus')) {
          replace.classList.remove('versus');
          }
      } else if (result === "player win") {
          replace.innerHTML = 'PLAYER 1 <br> WIN';
          replace.classList.add('result');
          replace.style.background = '#4C9654';
          if (replace.classList.contains('versus')) {
          replace.classList.remove('versus');
          }
      } else {
          replace.innerHTML = 'COM <br> WIN';
          replace.classList.add('result');
          replace.style.background = '#4C9654';
          if (replace.classList.contains('versus')) {
          replace.classList.remove('versus');
          }
      }
    }, [result])

const handleReload = (e) => {
  e.preventDefault();
  window.location.reload();
};

const connectUser = connect(mapStateToProps, mapDispatchToProps)(Game)

    return (
        <>
          <div className="body">
            <Nav>
              <NavItem className="games p-4 ms-2" id="back">
                <Link href="/home">
                  <Image src={back} alt="" />
                </Link>
              </NavItem>
              <NavItem className="logo p-2 ms-2" id="logo">
                <Image src={logo} alt="" />
              </NavItem>
              <NavItem className="headers p-4 ms-2">
                <h1>ROCK PAPER SCISSORS</h1>
              </NavItem>
            </Nav>
            <Container className="title">
              <Row className="user">
                <h2>Score: {score}</h2>
                <Col className="player p-3 d-flex">Player 1</Col>
                <Col className="com p-2 d-flex">COM</Col>
              </Row>
              <Row className="row batu">
                <Col className="col-4 d-flex">
                  <Image 
                    onClick={() => handlePicks("batu")}
                    className="option d-flex"
                    id="batu"
                    src="/batu.png" 
                    width={250}
                    height={186}
                    alt="" 
                    />
                </Col>
              <Col className="col-4"></Col>
                <Col className="col-4 d-flex">
                  <Image 
                    className="random d-flex"
                    id="batu2"
                    src="/batu.png"
                    width={188}
                    height={186}
                    alt="" 
                    />
                </Col>
              </Row>
              <Row className="row kertas">
                <Col className="col-4 d-flex">
                  <Image 
                    onClick={() => handlePicks("kertas")}
                    className="option d-flex"
                    id="kertas"
                    src="/kertas.png" 
                    width={188}
                    height={229}
                    alt="" 
                    />
                </Col>
                <Col className="d-flex versus">
                  <div className="replace d-flex">VS</div>
                </Col>
                <Col className="col-4 d-flex">
                  <Image 
                    className="random d-flex"
                    id="kertas2"
                    src="/kertas.png" 
                    width={188}
                    height={229}
                    alt="" 
                    />
                </Col>
              </Row>
              <Row className="row gunting">
                <Col className="col-4 d-flex">
                  <Image
                    onClick={() => handlePicks("gunting")}
                    className="option d-flex"
                    id="gunting"
                    src="/gunting.png" 
                    width={200}
                    height={231}
                    alt="" 
                    />
                </Col>
                <Col className="col-4"></Col>
                <Col className="col-4 d-flex">
                    <Image
                    className="random d-flex"
                    id="gunting2"
                    src="/gunting.png"
                    width={200}
                    height={231}
                    alt="" 
                    />
                </Col>
              </Row>
    
              <div className="refresh">
                <div className="col-md-6 offset-md-3">
                  <Image src={refresh} alt="" className="refresh-img" onClick={handleReload} />
                </div>
              </div>
            </Container>
          </div>
        </>
      );
    }
  }
    const mapStateToProps = (state) => {
      return {
        score: state.gameReducer.score,
        userChoice: state.gameReducer.userChoice
      };
    };
    const mapDispatchToProps = {
      updateScore,
      setUserChoice
    };
