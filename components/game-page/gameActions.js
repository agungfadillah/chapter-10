export const updateScore = (score) => {
    return {
      type: "UPDATE_SCORE",
      payload: score
    };
  };

export const setUserChoice = (choice) => {
    return {
      type: "SET_USER_CHOICE",
      payload: choice
    };
  };