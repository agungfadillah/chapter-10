const initialState = {
    score: 0,
    userChoice: null
  };
  
  const gameReducer = (state = initialState, action) => {
    switch (action.type) {
      case "UPDATE_SCORE":
        return {
          ...state,
          score: state.score + action.payload
        };
      case "SET_USER_CHOICE":
        return {
          ...state,
          userChoice: action.payload
        };
      default:
        return state;
    }
  };
  
  export default gameReducer;
  